from core.models import Restaurant,Categories,MenuItem
from django.shortcuts import render
from rest_framework import viewsets




class RestaurantViewset(viewsets.ViewSet):

    def get_menu(self, request, restaurant, table):
        # try:
        res = Restaurant.objects.get(id=restaurant)
        ct = Categories.objects.filter(menu_items__menu__restaurant_id=restaurant).distinct()
        mi = MenuItem.objects.filter(menu__restaurant_id=restaurant)
        return render(request, 'index.html', {'obj': res,
                                              'categories': ct,
                                              'items': mi
                                              }
                      )
        # except Exception as e:
        #     pass

