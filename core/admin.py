from django.contrib import admin

# Register your models here.
from core.models import Menu, MenuItem, Restaurant, Categories


class MenuItemInline(admin.TabularInline):
    model = MenuItem


class MenuAdmin(admin.ModelAdmin):
    inlines = [
        MenuItemInline,
    ]
    list_display = ('__str__',)


class MenuItemAdmin(admin.ModelAdmin):
    list_display = ('__str__',)


class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('__str__',)


class CategoriesAdmin(admin.ModelAdmin):
    pass

admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Categories, CategoriesAdmin)