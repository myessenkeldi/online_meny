from django.db import models

# Create your models here.
from core.qr_generator import generate_qr


class Restaurant(models.Model):
    tables = models.IntegerField()
    name = models.CharField(max_length=256)
    location = models.CharField(max_length=256)

    def __str__(self):
        return self.name

    def save(self, ):
        generate_qr(self.name, self.tables)


class Categories(models.Model):
    name = models.CharField(max_length=256)
    code = models.CharField(max_length=256, default='-')

    def __str__(self):
        return self.name


class Menu(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name='menus')

    def __str__(self):
        return 'Меню '+ self.restaurant.name


class MenuItem(models.Model):

    CURRENCY_CHOICES = (
        ('KZT', '₸'),
        ('USD', '$'),
        ('EURO', '€')
    )

    name = models.CharField(max_length=256, blank=False)
    price = models.IntegerField()
    category = models.ForeignKey(Categories, on_delete=models.CASCADE, related_name='menu_items')
    currency = models.CharField(default='KZT', choices=CURRENCY_CHOICES, max_length=32)
    picture = models.ImageField(upload_to='pictures/', blank=True, null=True)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE, related_name='menu_items')
    description = models.CharField(max_length=1024, default='-')

    def __str__(self):
        return self.name