from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from core import views
from core.viewsets import RestaurantViewset

get_menu = RestaurantViewset.as_view({
    'get': 'get_menu',
})

urlpatterns = [
    path('menu/', views.menu, name='menu'),
    url(r'^menu/(?P<restaurant>[0-9]+)/(?P<table>[0-9]+)/show/$', get_menu, name='get-menu'),
]
